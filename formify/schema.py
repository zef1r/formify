import abc
import collections

from . import exc, fields


class SchemaMeta(abc.ABCMeta):

    def __init__(cls, name, bases, attrs):
        cls.__fields__ = {}
        for name, value in attrs.items():
            if isinstance(value, fields.Field):
                value.attrname = name
                if value.key is None:
                    value.key = value.attrname
                cls.__fields__[value.key] = value


class Schema(collections.abc.MutableMapping, metaclass=SchemaMeta):
    __fields__ = {}

    def __init__(self, *args, **kwargs):
        self._data = {}
        keys = list(self.__fields__.keys())
        if len(args) > len(self.__fields__):
            raise TypeError(
                "__init__() takes at most {} argument(-s), "
                "got {}".format(len(self.__fields__), len(args)))
        for k, f in self.__fields__.items():
            if f.default is not None:
                self[k] = f.default
        for i, arg in enumerate(args):
            self[keys[i]] = arg
        for k, v in kwargs.items():
            if k not in self.__fields__:
                raise TypeError("__init__() has no attribute named {!r}".format(k))
            if k in self:
                raise TypeError("__init__() got multiple values for argument {!r}".format(k))
            self[k] = v

    def __contains__(self, key):
        return key in self._data

    def __iter__(self):
        return iter(self._data)

    def __len__(self):
        return len(self._data)

    def __setitem__(self, key, value):
        if key not in self.__fields__:
            raise TypeError("{!r} schema has no field {!r}".format(self.__class__.__name__, key))
        attrname = self.__fields__[key].attrname
        setattr(self, attrname, value)

    def __getitem__(self, key):
        if key not in self:
            raise KeyError(key)
        attrname = self.__fields__[key].attrname
        return getattr(self, attrname)

    def __delitem__(self, key):
        if key not in self:
            raise KeyError(key)
        attrname = self.__fields__[key].attrname
        delattr(self, attrname)

    def validate(self):
        errors = {}
        for key, field in self.__fields__.items():
            try:
                field.validate(self)
            except exc.ValidationError as e:
                errors[key] = e.message
        if errors:
            raise exc.ValidationError(errors)
