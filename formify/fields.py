import abc
import itertools

from . import exc


class Field(abc.ABC):
    attrname = None
    load_exc = (TypeError, ValueError)
    messages = {
        'required_missing': 'This field is required'
    }

    def __init__(self, key=None, default=None, optional=False):
        self.key = key
        self.default = default
        self.optional = optional
        self._preprocessors = []
        self._processors = []
        self._postprocessors = []
        self._validators = []
        self.add_validator(self.__class__.validate_required)

    def __set__(self, schema, value):
        schema._data[self.key] = self.load(schema, value)

    def __get__(self, schema, schema_class):
        if schema is None:
            return self
        else:
            return schema._data[self.key]

    def __delete__(self, schema):
        del schema._data[self.key]

    @property
    def default(self):
        if callable(self._default):
            return self._default()
        else:
            return self._default

    @default.setter
    def default(self, value):
        self._default = value

    def add_preprocessor(self, func):
        self._preprocessors.append(func)

    def add_postprocessor(self, func):
        self._postprocessors.append(func)

    def add_external_validator(self, func):

        def proxy(field, schema):
            return func(schema, field)

        self._validators.append(proxy)

    def add_processor(self, unbound_method):

        def proxy(schema, field, value):
            return unbound_method(field, schema, value)

        self._processors.append(proxy)

    def add_validator(self, unbound_method):
        self._validators.append(unbound_method)

    def load(self, schema, value):
        try:
            processors = itertools.chain(
                self._preprocessors, self._processors, self._postprocessors)
            for func in processors:
                value = func(schema, self, value)
        except self.load_exc as e:
            raise exc.ParsingError(original_exc=e)
        else:
            return value

    def validate(self, schema):
        errors = []
        for func in self._validators:
            error = func(self, schema)
            if error is not None:
                errors.append(error)
        if errors:
            raise exc.ValidationError(errors)

    def format_message(self, message_key, **message_params):
        return self.messages[message_key].format(**message_params)

    def validate_required(self, schema):
        if not self.optional and self.key not in schema:
            return self.format_message('required_missing')


class Type(Field):

    def __init__(self, type_, **kwargs):
        super().__init__(**kwargs)
        self.type_ = type_
        self.add_processor(self.__class__.parse)

    def parse(self, schema, value):
        return self.type_(value)
