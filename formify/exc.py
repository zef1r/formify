class FormifyError(Exception):
    pass


class ParsingError(FormifyError):

    def __init__(self, original_exc):
        self.original_exc = original_exc

    def __str__(self):
        return str(self.original_exc)


class ValidationError(FormifyError):

    def __init__(self, message):
        self.message = message
