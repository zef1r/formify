def preprocesses(*fields):

    def decorator(func):
        for field in fields:
            field.add_preprocessor(func)

    return decorator


def postprocesses(*fields):

    def decorator(func):
        for field in fields:
            field.add_postprocessor(func)

    return decorator


def validates(*fields):

    def decorator(func):

        def proxy(schema, field):
            return func(schema, field)

        def optional_proxy(schema, field):
            if field.key in schema:
                return func(schema, field)

        for field in fields:
            field.add_external_validator(
                optional_proxy if field.optional else proxy)

    return decorator
