import pytest

from formify import (
    exc, fields, preprocesses, postprocesses, validates, Schema
)


class UUT(Schema):
    foo = fields.Type(str)
    bar = fields.Type(str)
    baz = fields.Type(str, optional=True)


class TestSchema:

    def test_if_schema_created_without_args__then_no_fields_are_initialized(self):
        uut = UUT()
        assert list(uut) == []

    def test_if_schema_created_with_one_positional_arg__then_first_field_is_initialized(self):
        uut = UUT('spam')
        assert list(uut) == ['foo']
        assert uut.foo == 'spam'

    def test_if_schema_created_with_two_positional_args__then_first_two_fields_are_initialized(self):
        uut = UUT('a', 'b')
        assert list(uut) == ['foo', 'bar']
        assert uut.foo == 'a'
        assert uut.bar == 'b'

    def test_if_schema_created_with_keyword_args__then_fields_matching_given_keys_are_initialized(self):
        uut = UUT(bar='b')
        assert list(uut) == ['bar']
        assert uut.bar == 'b'

    def test_create_schema_using_both_positional_and_named_args(self):
        uut = UUT('a', bar='b')
        assert list(uut) == ['foo', 'bar']
        assert uut.foo == 'a'
        assert uut.bar == 'b'

    def test_when_argument_is_given_twice__then_fail_with_type_error(self):
        with pytest.raises(TypeError) as excinfo:
            UUT('a', foo='b')
        assert str(excinfo.value) == "__init__() got multiple values for argument 'foo'"

    def test_when_too_many_positional_args_given__then_fail_with_type_error(self):
        with pytest.raises(TypeError) as excinfo:
            UUT('a', 'b', 'c', 'd')
        assert str(excinfo.value) == "__init__() takes at most 3 argument(-s), got 4"

    def test_when_given_keyword_arg_is_not_a_valid_field__then_fail_with_type_error(self):
        with pytest.raises(TypeError) as excinfo:
            UUT(spam='abc')
        assert str(excinfo.value) == "__init__() has no attribute named 'spam'"

    @pytest.mark.parametrize('args, expected_len', [
        (tuple(), 0),
        (('a',), 1),
        (('a', 'b'), 2),
    ])
    def test_schema_length_is_equal_to_number_of_attributes_set(self, args, expected_len):
        uut = UUT(*args)
        assert len(uut) == expected_len

    def test_setting_a_key_for_which_there_is_no_field_defined_causes_type_error(self):
        uut = UUT()
        with pytest.raises(TypeError) as excinfo:
            uut['non_existing'] = 'more spam'
        assert str(excinfo.value) == "'UUT' schema has no field 'non_existing'"

    def test_getting_a_non_existing_key_causes_key_error(self):
        uut = UUT()
        with pytest.raises(KeyError) as excinfo:
            _ = uut['non_existing']
        assert str(excinfo.value) == "'non_existing'"

    def test_getting_an_unset_key_causes_key_error(self):
        uut = UUT()
        with pytest.raises(KeyError) as excinfo:
            _ = uut['foo']
        assert str(excinfo.value) == "'foo'"

    def test_deleting_a_non_existing_key_causes_key_error(self):
        uut = UUT()
        with pytest.raises(KeyError) as excinfo:
            del uut['non_existing']
        assert str(excinfo.value) == "'non_existing'"

    def test_deleting_an_unset_key_causes_key_error(self):
        uut = UUT()
        with pytest.raises(KeyError) as excinfo:
            del uut['foo']
        assert str(excinfo.value) == "'foo'"

    def test_remove_value_from_schema_by_deleting_key(self):
        uut = UUT('a')
        assert list(uut) == ['foo']
        del uut['foo']
        assert list(uut) == []

    def test_if_required_fields_are_not_set__then_validation_fails(self):
        uut = UUT()
        with pytest.raises(exc.ValidationError) as excinfo:
            uut.validate()
        e = excinfo.value
        assert e.message == {
            'foo': ['This field is required'],
            'bar': ['This field is required']
        }

    def test_if_schema_has_preprocessors_defined__they_are_executed_before_value_is_converted(self):

        class UUT(Schema):
            foo = fields.Type(int)

            @preprocesses(foo)
            def convert_to_int(self, field, value):
                return len(value)

        uut = UUT()
        uut.foo = 'spam'
        assert uut.foo == 4

    def test_if_schema_has_postprocessors_defined__they_are_executed_after_value_is_converted(self):

        class UUT(Schema):
            foo = fields.Type(str)

            @postprocesses(foo)
            def multiply_times_two(self, field, value):
                return value * 2

        uut = UUT()
        uut.foo = 123
        assert uut.foo == '123123'

    def test_if_schema_has_validators_defined__they_are_executed_when_validate_is_called(self):

        class UUT(Schema):
            foo = fields.Type(int)

            @validates(foo)
            def disallow_less_than_zero(self, field):
                if field.key in self:
                    if self[field.key] < 0:
                        return 'less than zero'

        uut = UUT()
        uut.foo = -1
        assert uut.foo == -1
        with pytest.raises(exc.ValidationError) as excinfo:
            uut.validate()
        e = excinfo.value
        assert e.message == {'foo': ['less than zero']}

    def test_if_schema_has_validators_defined_for_optional_fields__they_are_executed_when_validate_is_called_and_field_has_value(self):

        class UUT(Schema):
            foo = fields.Type(int, optional=True)

            @validates(foo)
            def run_only_if_value_present(self, field):
                return 'invalid'

        uut = UUT()
        uut.validate()
        uut.foo = -1
        assert uut.foo == -1
        with pytest.raises(exc.ValidationError) as excinfo:
            uut.validate()
        e = excinfo.value
        assert e.message == {'foo': ['invalid']}


