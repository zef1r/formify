import pytest

from formify import exc, fields, Schema


class TestType:

    def make_schema_class(self, *args, **kwargs):

        class _Schema(Schema):
            uut = fields.Type(*args, **kwargs)

        return _Schema

    def make_schema(self, *args, **kwargs):
        schema_class = self.make_schema_class(*args, **kwargs)
        return schema_class()

    def test_if_field_has_default__it_is_used_to_initialize_schema(self):
        schema = self.make_schema(int, default=123)
        assert list(schema) == ['uut']
        assert schema.uut == 123

    def test_is_field_has_callable_default__it_is_used_to_initialize_schema_with_value_it_returns(self):
        schema = self.make_schema(int, default=lambda: 123)
        assert list(schema) == ['uut']
        assert schema.uut == 123

    def test_parse_value_passed_to_constructor(self):
        schema_class = self.make_schema_class(int)
        schema = schema_class('123')
        assert schema.uut == 123

    def test_parse_value_set_via_attribute(self):
        schema = self.make_schema(int)
        schema.uut = '123'
        assert schema.uut == 123

    def test_parse_value_set_via_item(self):
        schema = self.make_schema(int)
        schema['uut'] = '123'
        assert schema['uut'] == 123
        assert schema.uut == 123

    def test_if_field_has_explicit_key_defined__it_is_used_instead_of_attribute_name(self):
        schema = self.make_schema(int, key='spam')
        schema['spam'] = '123'
        assert list(schema) == ['spam']
        assert schema['spam'] == 123
        assert schema.uut == 123  # Original attribute is available as well

    def test_if_field_is_unable_to_convert_value__then_parsing_error_is_raised(self):
        schema = self.make_schema(int)
        with pytest.raises(exc.ParsingError) as excinfo:
            schema.uut = 'not_an_int'
        e = excinfo.value
        assert str(e.original_exc) == "invalid literal for int() with base 10: 'not_an_int'"
